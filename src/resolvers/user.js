import Joi from 'joi'
import mongoose from 'mongoose'
import { UserInputError } from 'apollo-server-express'
import { SignUp } from '../schemas'
import User from '../models/user'
import Channel from '../models/channel'
import bcrypt from 'bcryptjs'
import jwt from 'jsonwebtoken';


export default {
  Query: {
    users: (root, args, context, info) => {
      return User.find({})
    },
    user: (root, { id }, context, info) => {
      if (!mongoose.Types.ObjectId.isValid(id)) {
        throw new UserInputError(`${id} is not a valid user ID.`)
      }
      return User.findById(id)
    },
    channels: (root, { id }, context, info) => {
      return Channel.find({})
    },
  },
  Mutation: {
    addNewChannel: async (root, args, context, info) => {
      Channel.create(args);
      return {
        bool: true,
        token: '',
        message: "Requette good.",
      };
    },
    newMessage: async (root, args, context, info) => {
      const newMessage = {
        message: args.message,
        sendby: args.sendby,
        time: args.time
      }
      await Channel.findOneAndUpdate(
        { name: args.channel },
        {  $push: { messages: newMessage } },
        { new: true });
      return {
        bool: true,
        token: '',
        message: "Requette good.",
      };
    },
    signUp: async (root, args, context, info) => {
      const user = await User.findOne({ username: args.email });
      if (user) {
        return {
          bool: false,
          token: '',
          message: "L'email existe ou n'est pas correct.",
        };
      } else {
        await Joi.validate(args, SignUp, { abortEarly: false })
        User.create(args)

        return {
          bool: true,
          token: '',
          message: "Inscription fini",
        };
      }
    },
    login: async (root, args, context, info) => {
      const user = await User.findOne({ username: args.username });
      let userToken = '';
      if (user) {
        const payload = {
          id: user.id,
          name: user.name,
        }
        jwt.sign(payload, 'secret', {
          expiresIn: 3600
        }, (err, token) => {
          if (err) {
            console.error('There is some error in token', err);
            return {
              bool: false,
              token: '',
              message: "Erreur du serveur, contactez l'administration."
            };
          }
          else {
            userToken = token;
          }
        });
        const isMatch = await bcrypt.compare(args.password, user.password);
        return {
          bool: isMatch,
          token: userToken,
          message: isMatch ? "Connexion reussi !" : "Connexion failed ? Raison inconnu."
        };
      } else {
        return {
          bool: false,
          token: userToken,
          message: "Nom d'utilisateur inconnu."
        };
      }
    }
  }

}
