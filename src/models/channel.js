import mongoose from 'mongoose'

const channelSchema = new mongoose.Schema({
  name: {
    type: String
  },
  owner: {
    type: String
  },
  messages: [{
      message: String,
      sendby: String,
      time: String,
    }]
  
}, {
  timestamps: true
})

const Channel = mongoose.model('Channel', channelSchema)

export default Channel
