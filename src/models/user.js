import mongoose from 'mongoose'
import bcrypt from 'bcryptjs'


const userSchema = new mongoose.Schema({
  email: {
    type: String
  },
  username: {
    type: String
  },
  name: String,
  password: String
}, {
  timestamps: true
})

userSchema.pre('save',  function (next) {
  let user = this;

  if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(10, (err, salt) => {
          if (err) {
              return next(err);
          }

          bcrypt.hash(user.password, salt, (err, hash) => {
              if (err) {
                  return next(err);
              }

              user.password = hash;
              next();
          });
      });
  } else {
      next();
  }
});

userSchema.statics.doesntExist = async function (options) {
  return await this.where(options).countDocuments() === 0
}

const User = mongoose.model('User', userSchema)

export default User
