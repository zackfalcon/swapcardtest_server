import root from './root'
import user from './user'
import chat from './chat'

export default [
  root,
  user,
  chat
]
