import { gql } from 'apollo-server-express'

export default gql`
  extend type Query {
    channels: [Channel!]!
  }
  
  extend type Mutation {
    addNewChannel(name: String!, owner: String!): Error!
    newMessage(sendby: String!, channel: String!, time: String!, message: String!): Error!
  }

  type Message {
    message: String,
    sendby: String,
    time: String
  }

  type Channel {
    id: ID!
    name: String!
    owner: String!
    messages: [Message]
  }
`
