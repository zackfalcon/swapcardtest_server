# base image
FROM node:9.6.1

RUN mkdir /usr/src/swapcardtest_server
WORKDIR /usr/src/swapcardtest_server

ENV PATH /usr/src/swapcardtest_server/node_modules/.bin:$PATH

COPY package.json /usr/src/swapcardtest_server/package.json

EXPOSE 4000

RUN npm install
RUN npm install -g nodemon

CMD npm start